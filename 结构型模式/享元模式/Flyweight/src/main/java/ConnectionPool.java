import java.util.Vector;

/**
 * Created by Jao on 15/11/29.
 */
public class ConnectionPool {
    private static ConnectionPool connectionPool;

    private Vector<Connection> pool;

    public static ConnectionPool getConnectionPool() {
        if(connectionPool == null){
            connectionPool = new ConnectionPool();
            connectionPool.pool = new Vector<Connection>();
            connectionPool.pool.add(new Connection());
            connectionPool.pool.add(new Connection());
            connectionPool.pool.add(new Connection());
            connectionPool.pool.add(new Connection());
            connectionPool.pool.add(new Connection());
            connectionPool.pool.add(new Connection());
        }
        return connectionPool;
    }

    public synchronized void release(Connection conn) {
        pool.add(conn);
    }

    public synchronized Connection getConnection() {
        if (pool.size() > 0) {
            Connection conn = pool.get(0);
            pool.remove(conn);
            return conn;
        } else {
            return null;
        }
    }
}
