/**
 * Created by Jao on 15/11/28.
 */
public class Proxy implements IFindUser {
    private FindUserXml source = new FindUserXml();

    public void findUser() {
        source.findUser();
    }
}
