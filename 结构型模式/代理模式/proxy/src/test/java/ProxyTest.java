import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class ProxyTest {
    @Category(ProxyTest.class)
    @Test
    public void testProxy() {
        System.out.println("代理模式是可以在间接访问对象的同时,要其前或后,添加其它的逻辑代码.对原来逻辑进行添加其它逻辑,最终生成新的逻辑.即:对类的方法添加一些额外的逻辑,生成新的方法逻辑.");
        IFindUser findUser = new Proxy();
        findUser.findUser();
    }
}
