/**
 * Created by Jao on 15/11/28.
 */
public class FindUserFacade {
    private IFindUser findUserXml = new FindUserXml();
    private IFindUser findUserJson = new FindUserJson();

    public void findUser(){
        findUserXml.findUser();
        findUserJson.findUser();
    }
}
