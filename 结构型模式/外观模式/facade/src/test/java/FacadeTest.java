import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class FacadeTest {
    @Category(FacadeTest.class)
    @Test
    public void testFacade() {
        System.out.println("外观模式是为了解决类与类之家的依赖关系的，像spring一样，可以将类和类之间的关系配置到配置文件中，而外观模式就是将他们的关系放在一个Facade类中，降低了类类之间的耦合度");

        FindUserFacade facade = new FindUserFacade();
        facade.findUser();
    }
}
