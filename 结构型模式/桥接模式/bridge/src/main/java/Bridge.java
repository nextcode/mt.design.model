/**
 * Created by Jao on 15/11/28.
 */
public abstract class Bridge {
    private IFindUser findUser;

    public IFindUser getFindUser() {
        return findUser;
    }

    public void setFindUser(IFindUser findUser) {
        this.findUser = findUser;
    }

    public void findUser(){
        findUser.findUser();
    }
}
