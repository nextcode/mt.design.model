import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class FacadeTest {
    @Category(FacadeTest.class)
    @Test
    public void testFacade() {
        System.out.println("桥接模式就是把抽象和其具体实现分开，使他们可以各自独立的变化");

        Bridge bridge = new ShenZhenBridge();

        IFindUser findUserXml = new FindUserXml();
        bridge.setFindUser(findUserXml);
        bridge.findUser();

        IFindUser finduserJson = new FindUserJson();
        bridge.setFindUser(finduserJson);
        bridge.findUser();
    }
}
