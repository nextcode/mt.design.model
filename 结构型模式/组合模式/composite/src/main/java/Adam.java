import java.util.Vector;

/**
 * Created by Jao on 15/11/29.
 */
public class Adam {
    private String name;
    private Vector<Adam> adams = new Vector<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector<Adam> getAdams() {
        return adams;
    }

    public void setAdams(Vector<Adam> adams) {
        this.adams = adams;
    }
}
