import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Decorator Tester.
 *
 * @author mt
 * @version 1.0
 * @since <pre>11/28/2015</pre>
 */
public class DecoratorTest {
    @Category(DecoratorTest.class)
    @Test
    public void testDecorator(){
        System.out.println("组合模式又叫整体部分,树形结构就是典型的组合模式");

        Adam adamA = new Adam();
        adamA.setName("adamA");

        Adam adamB = new Adam();
        adamB.setName("adamA");

        Adam adam = new Adam();
        adam.setName("adam");
        adam.getAdams().add(adamA);
        adam.getAdams().add(adamB);
    }
}
