/**
 * Created by Jao on 15/11/28.
 */
public class Decorator implements IFindUser {
    private IFindUser findUser;

    public Decorator(IFindUser findUser) {
        super();
        this.findUser = findUser;
    }

    @Override
    public void findUser() {
        this.findUser.findUser();
    }
}
