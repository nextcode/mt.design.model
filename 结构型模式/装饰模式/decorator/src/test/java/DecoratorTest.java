import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Decorator Tester.
 *
 * @author mt
 * @version 1.0
 * @since <pre>11/28/2015</pre>
 */
public class DecoratorTest {

    @Category(DecoratorTest.class)
    @Test
    public void testDecorator(){
        System.out.println("修饰模式: 顾名思义，装饰模式就是给一个对象增加一些新的功能，而且是动态的，要求装饰对象和被装饰对象实现同一个接口，装饰对象持有被装饰对象的实例");
        IFindUser findUser = new FindUserXml();
        Decorator decorator = new Decorator(findUser);

        decorator.findUser();
    }
}
