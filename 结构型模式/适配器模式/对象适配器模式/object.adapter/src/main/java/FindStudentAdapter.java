/**
 * Created by Jao on 15/11/28.
 */
public class FindStudentAdapter implements IFindStudentExpand {
    private FindStudent findStudent = new FindStudent();

    @Override
    public void findStudentXmlData() {
        findStudent.findStudentXmlData();
    }

    @Override
    public void findStudentJsonData() {
        System.out.println("Json格式的学生信息集");
    }
}
