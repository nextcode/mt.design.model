/**
 * Created by Jao on 15/11/28.
 */
public interface IFindStudentExpand {
    void findStudentXmlData();
    void findStudentJsonData();
}
