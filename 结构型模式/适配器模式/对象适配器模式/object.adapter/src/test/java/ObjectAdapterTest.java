import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class ObjectAdapterTest {
    @Category(ObjectAdapterTest.class)
    @Test
    public void testObjectAdapterTest() {
        System.out.println("对象适配器模式:该模式与类适配器模式相同,只是适配器里不再继承原始类,而是依赖了原始类的实例");
        IFindStudentExpand expand = new FindStudentAdapter();
        expand.findStudentXmlData();
        expand.findStudentJsonData();
    }
}
