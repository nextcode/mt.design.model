import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class InterfaceAdapterTest {
    @Category(InterfaceAdapterTest.class)
    @Test
    public void test() {
        System.out.println("接口适配器模式:由于子类对于接口中的一些方法不感兴趣, 所以让抽象类作为适配器去实现接口");
        IFindStudentExpand expand1 = new FindStudentJson();
        IFindStudentExpand expand2 = new FindStudentXml();

        expand1.findStudentXmlData();
        expand1.findStudentJsonData();

        expand2.findStudentXmlData();
        expand2.findStudentJsonData();
    }
}
