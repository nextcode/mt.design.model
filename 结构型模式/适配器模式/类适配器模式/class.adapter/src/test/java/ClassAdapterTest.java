import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * FindStudent Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>11/28/2015</pre>
 */
public class ClassAdapterTest {

    @Category(ClassAdapterTest.class)
    @Test
    public void testClassAdapterTest() {
        System.out.println("类适配器模式:适配器模式将某个类的接口转换成客户端期望的另一个接口表示，目的是消除由于接口不匹配所造成的类的兼容性问题。");
        IFindStudentExpand expand = new FindStudentAdapter();
        expand.findStudentJsonData();
        expand.findStudentXmlData();
    }
} 
