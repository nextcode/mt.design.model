/**
 * Created by Jao on 15/12/3.
 */
public interface Mediator {
    void createMediator();
    void workAll();
}