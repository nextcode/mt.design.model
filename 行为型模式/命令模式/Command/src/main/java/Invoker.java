/**
 * Created by Jao on 15/12/1.
 */
public class Invoker {

    private ICommand command;

    public Invoker(ICommand command) {
        this.command = command;
    }

    public void action() {
        command.exe();
    }
}
