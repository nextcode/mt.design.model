import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/28.
 */
public class MementoTest {
    @Category(MementoTest.class)
    @Test
    public void testMemento() {
        System.out.println("备忘录主要目的是保存一个对象的某个状态,以便在适当的时候恢复对象.");

        // 创建原始类
        Original origi = new Original("egg");

        // 创建备忘录
        Storage storage = new Storage(origi.createMemento());

        // 修改原始类的状态
        System.out.println("初始化状态为：" + origi.getValue());
        origi.setValue("niu");
        System.out.println("修改后的状态为：" + origi.getValue());

        // 回复原始类的状态
        origi.restoreMemento(storage.getMemento());
        System.out.println("恢复后的状态为：" + origi.getValue());
    }
}
