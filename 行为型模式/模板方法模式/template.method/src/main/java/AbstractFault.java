/**
 * Created by Jao on 15/11/30.
 */
public abstract class AbstractFault {
    public void subscribeFault(Exception ex) {
        System.out.println(ex.getMessage());
        System.out.println("原因如下:");

        subscribeFaultCause();
    }

    public abstract void subscribeFaultCause();
}
