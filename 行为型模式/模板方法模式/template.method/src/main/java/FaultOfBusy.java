import java.util.stream.Stream;

/**
 * Created by Jao on 15/11/30.
 */
public class FaultOfBusy extends AbstractFault {
    public FaultOfBusy(Exception... exceptions) {
        this.exceptions = exceptions;
    }

    private Exception[] exceptions;

    @Override
    public void subscribeFaultCause() {
        System.out.println("服务器很忙");
        Stream.of(exceptions).forEach(s -> {
            System.out.println(s.getMessage());
        });
    }
}
