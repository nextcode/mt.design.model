import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/30.
 */
public class TemplateMethodTest {
    @Category(TemplateMethodTest.class)
    @Test
    public void testStraegy() throws InterruptedException {
        System.out.println("策略模式定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，且算法的变化不会影响到使用算法的客户");

        FaultOfBusy subscribeFaultOfBusy = new FaultOfBusy(
                new Exception("CPU使用率为55%"),
                new Exception("网络带宽使用率为95%."));

        AbstractFault fault  = subscribeFaultOfBusy;
        fault.subscribeFault(new Exception("预约场馆失败"));
    }
}
