import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/12/1.
 */
public class StateTest {
    @Category(StateTest.class)
    @Test
    public void testState() {
        System.out.println("状态模式就是当对象的状态改变时，同时改变其行为");

        State state = new State();
        Context context = new Context(state);

        //设置第一种状态
        state.setValue("state1");
        context.method();

        //设置第二种状态
        state.setValue("state2");
        context.method();
    }
}
