import java.util.Collection;
import java.util.Date;

/**
 * Created by Jao on 15/11/30.
 */
public class DebarUsingTime implements IDebarStrategy {
    @Override
    public void debarStrategy(Collection<Date> date) {
        System.out.println("排除已经占用的时间");
    }
}
