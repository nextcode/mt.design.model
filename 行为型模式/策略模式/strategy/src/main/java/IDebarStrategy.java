import java.util.Collection;
import java.util.Date;

/**
 * Created by Jao on 15/11/30.
 */
public interface IDebarStrategy {
    void debarStrategy(Collection<Date> date);
}
