import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/11/30.
 */
public class StrategyTest {
    @Category(StrategyTest.class)
    @Test
    public void testStraegy() throws InterruptedException {
        System.out.println("策略模式定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，且算法的变化不会影响到使用算法的客户");

        IDebarStrategy strategy1 = new DebarExceptTime();
        strategy1.debarStrategy(null);

        IDebarStrategy strategy2  = new DebarUsingTime();
        strategy2.debarStrategy(null);
    }
}
