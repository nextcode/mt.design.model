import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by Jao on 15/12/1.
 */
public abstract class AbstractISubject implements ISubject {

    private Vector<IObserver> vector = new Vector<>();
    @Override
    public void add(IObserver IObserver) {
        vector.add(IObserver);
    }

    @Override
    public void del(IObserver IObserver) {
        vector.remove(IObserver);
    }

    @Override
    public void notifyObservers() {
        Enumeration<IObserver> enumo = vector.elements();
        while(enumo.hasMoreElements()){
            enumo.nextElement().update();
        }
    }
}