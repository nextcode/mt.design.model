/**
 * Created by Jao on 15/12/1.
 */
public interface ISubject {
    /*增加观察者*/
    void add(IObserver IObserver);

    /*删除观察者*/
    void del(IObserver IObserver);

    /*通知所有的观察者*/
    void notifyObservers();

    /*自身的操作*/
    void operation();
}
