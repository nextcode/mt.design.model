/**
 * Created by Jao on 15/12/1.
 */
public class MySubject extends AbstractISubject {

    @Override
    public void operation() {
        System.out.println("update self!");
        notifyObservers();
    }
}
