/**
 * Created by Jao on 15/12/1.
 */
public class Observer2 implements IObserver {

    @Override
    public void update() {
        System.out.println("observer2 has received!");
    }

}
