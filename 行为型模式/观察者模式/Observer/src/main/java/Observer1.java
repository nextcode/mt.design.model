/**
 * Created by Jao on 15/12/1.
 */
public class Observer1 implements IObserver {

    @Override
    public void update() {
        System.out.println("observer1 has received!");
    }
}
