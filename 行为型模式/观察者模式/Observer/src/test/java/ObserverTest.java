import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/12/1.
 */
public class ObserverTest {
    @Category(ObserverTest.class)
    @Test
    public void testObserver(){
        System.out.println("观察者模式属于行为型模式，其意图是定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。这一个模式的关键对象是目标（Subject）和观察者（Observer）。一个目标可以有任意数目的依赖它的观察者，一旦目标的状态发生改变，所有的观察者都得到通知，作为对这个通知的响应，每个观察者都将查询目标以使其状态与目标的状态同步。");

        ISubject sub = new MySubject();
        sub.add(new Observer1());
        sub.add(new Observer2());

        sub.operation();
    }
}
