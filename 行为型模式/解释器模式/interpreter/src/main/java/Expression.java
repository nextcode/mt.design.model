/**
 * Created by Jao on 15/12/3.
 */
public interface Expression {
    public int interpret(Context context);
}
