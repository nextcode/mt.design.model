import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/12/3.
 */
public class InterpreterTest {
    @Category(InterpreterTest.class)
    @Test
    public void testInterpreter() {
        int result = new Minus().interpret((new Context(new Plus()
                .interpret(new Context(9, 2)), 8)));
        System.out.println(result);
    }
}
