/**
 * Created by Jao on 15/12/2.
 */
public class VisitorA implements Visitor {
    @Override
    public void visit(NodeA node) {
        System.out.println("VisitorA：" + node.operation());
    }

    @Override
    public void visit(NodeB node) {
        System.out.println("VisitorA：" + node.operation());
    }
}
