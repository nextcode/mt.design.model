/**
 * Created by Jao on 15/12/3.
 */
public class VisitorB implements Visitor {
    @Override
    public void visit(NodeA node) {
        System.out.println("VisitorB：" + node.operation());
    }

    @Override
    public void visit(NodeB node) {
        System.out.println("VisitorB：" + node.operation());
    }
}
