import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/12/2.
 */
public class VisitorTest {
    @Category(VisitorTest.class)
    @Test
    public void testVisitor() {
        System.err.println("访问者模式的目的是封装一些施加于某种数据结构元素之上的操作。一旦这些操作需要修改的话，接受这个操作的数据结构则可以保持不变。");
        System.out.println("访问者模式适用于数据结构相对未定的系统，它把数据结构和作用于结构上的操作之间的耦合解脱开，使得操作集合可以相对自由地演化。");
        System.out.println("1.抽象访问者(Visitor)角色：声明了一个或者多个方法操作，形成所有的具体访问者角色必须实现的接口。\n" +
                "2.具体访问者(ConcreteVisitor)角色：实现抽象访问者所声明的接口，也就是抽象访问者所声明的各个访问操作。\n" +
                "3.抽象节点(Node)角色：声明一个接受操作，接受一个访问者对象作为一个参数。\n" +
                "4.具体节点(ConcreteNode)角色：实现了抽象节点所规定的接受操作。\n" +
                "5.结构对象(ObjectStructure)角色：有如下的责任，可以遍历结构中的所有元素；如果需要，提供一个高层次的接口让访问者对象可以访问每一个元素；如果需要，可以设计成一个复合对象或者一个聚集，如List或Set。");
        System.out.println("参考地址:http://blog.csdn.net/m13666368773/article/details/7711326");
        // 创建一个结构对象
        ObjectStructure os = new ObjectStructure();
        // 给结构增加一个节点
        os.add(new NodeA());
        // 给结构增加一个节点
        os.add(new NodeB());
        // 创建一个访问者
        Visitor visitor = new VisitorA();
        os.action(visitor);
    }
}