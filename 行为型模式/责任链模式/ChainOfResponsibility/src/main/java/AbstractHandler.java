/**
 * Created by Jao on 15/12/1.
 */
public abstract class AbstractHandler {

    private IHandler handler;

    public IHandler getHandler() {
        return handler;
    }

    public void setHandler(IHandler handler) {
        this.handler = handler;
    }

}
