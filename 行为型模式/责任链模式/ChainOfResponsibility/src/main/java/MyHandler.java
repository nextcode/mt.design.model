/**
 * Created by Jao on 15/12/1.
 */
public class MyHandler extends AbstractHandler implements IHandler {

    private String name;

    public MyHandler(String name) {
        this.name = name;
    }

    @Override
    public void operator() {
        System.out.println(name + "deal!");
        if (getHandler() != null) {
            getHandler().operator();
        }
    }
}
