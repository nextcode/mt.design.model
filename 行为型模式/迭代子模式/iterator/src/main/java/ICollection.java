/**
 * Created by Jao on 15/12/1.
 */
public interface ICollection {
    public IIterator iterator();

    /*取得集合元素*/
    public Object get(int i);

    /*取得集合大小*/
    public int size();
}
