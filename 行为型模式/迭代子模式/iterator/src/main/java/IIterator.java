/**
 * Created by Jao on 15/12/1.
 */
public interface IIterator {
    //前移
    public Object previous();

    //后移
    public Object next();

    //是否存在下一个元素
    public boolean hasNext();

    //取得第一个元素
    public Object first();
}
