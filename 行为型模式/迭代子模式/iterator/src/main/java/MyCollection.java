/**
 * Created by Jao on 15/12/1.
 */
public class MyCollection implements ICollection {
    public String string[] = {"A","B","C","D","E"};
    @Override
    public IIterator iterator() {
        return new MyIterator(this);
    }

    @Override
    public Object get(int i) {
        return string[i];
    }

    @Override
    public int size() {
        return string.length;
    }
}
