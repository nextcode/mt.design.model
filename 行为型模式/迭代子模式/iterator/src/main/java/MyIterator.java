/**
 * Created by Jao on 15/12/1.
 */
public class MyIterator implements IIterator {
    private ICollection collection;
    private int pos = -1;

    public MyIterator(ICollection collection) {
        this.collection = collection;
    }

    @Override
    public Object previous() {
        if (pos > 0) {
            pos--;
        }
        return collection.get(pos);
    }

    @Override
    public Object next() {
        if (pos < collection.size() - 1) {
            pos++;
        }
        return collection.get(pos);
    }

    @Override
    public boolean hasNext() {
        if (pos < collection.size() - 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object first() {
        return collection.get(0);
    }
}