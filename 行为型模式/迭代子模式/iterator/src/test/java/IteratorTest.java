import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jao on 15/12/1.
 */
public class IteratorTest {
    @Category(IteratorTest.class)
    @Test
    public void testIterator() {
        System.out.println("迭代器模式属于行为型模式，其意图是提供一种方法顺序访问一个聚合对象中得各个元素，而又不需要暴露该对象的内部表示。");

        ICollection collection = new MyCollection();
        IIterator iterator = collection.iterator();

        System.out.println("第一个元素:" + iterator.first());
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("最后一个向前一个元素:" + iterator.previous());
    }
}